package com.company.apptoken.security;

import com.company.apptoken.entity.User;
import com.company.apptoken.repository.RoleRepository;
import com.company.apptoken.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.Collections;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

//    public ApiResponse addUser(ReqUser reqUser) {
//        if (!userRepository.existsByPhoneNumber(reqUser.getPhoneNumber())) {
//            User user = new User();
//            user.setFirstName(reqUser.getFirstName());
//            user.setLastName(reqUser.getLastName());
//            user.setMiddleName(reqUser.getMiddleName());
//            user.setPhoneNumber(reqUser.getPhoneNumber());
//            user.setBirthDate(reqUser.getBirthDate());
//            user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
//            user.setRoles(Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_USER)));
//            userRepository.save(user);
//            return new ApiResponse("User ro'yxatdan o'tkazildi", true);
//        }
//        return new ApiResponse("Bunday telefon raqamli user sistemada mavjud", false);
//    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByUsername(s).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

    public UserDetails loadUserByUserId(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }
}
