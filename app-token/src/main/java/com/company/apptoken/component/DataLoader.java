package com.company.apptoken.component;

import com.company.apptoken.entity.User;
import com.company.apptoken.repository.RoleRepository;
import com.company.apptoken.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements CommandLineRunner {
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")){
                userRepository.save(
                        new User(
                            "Coder",
                            "coder",
                             passwordEncoder.encode("123"),
                             roleRepository.findAll()
                        )
                );
        }
    }
}
