package com.company.apptoken.entity.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_MODER,
    ROLE_ADMIN
}
