package com.company.apptoken.repository;

import com.company.apptoken.entity.Role;
import com.company.apptoken.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

}
